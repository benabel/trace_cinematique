# trace_cinematique

Module python permettant le tracé de vecteurs cinématiques.

Le module contient la fonction `trace_vecteurs` qui permet le tracé aisé à
partir d'un fichier `csv` de vecteurs cinématiques:

- vecteur vitesse;
- vecteur variation de vitesse.

Voir l'article de
[blog](https://lyceum.fr/blog/2020-01-31-trace-de-vecteurs-cinematiques-en-python/)
expliquant son fonctionnement sur <https://lyceum.fr>.

## Utilisation

Après avoir fait les pointages, les élèves exportent leurs pointages au format
`csv` à partir de [Logger Pro©](https://www.vernier.com/product/logger-pro-3/)
dans le même dossier que le fichier `trace_cinematique` .

Ensuite dans un fichier nommé par exemple `fichier_eleve.py` placé toujours dans
le même dossier, les élèves importent la fonction `trace_vecteurs` et donnent le
nom du fichier csv en argument:

## Licence

Le code mis à disposition dans le module trace_cinematique est sous licence
[ISC](https://fr.wikipedia.org/wiki/Licence_ISC), une licence libre qui permet
de l'utiliser et le modifier comme bon vous semble.

## Contribuer

Si vous avez des propositions pour l'amélioration de ce module n'hésitez pas à
ouvrir un [ticket](https://framagit.org/benabel/trace_cinematique/issues) sur le
dépôt framagit du projet.